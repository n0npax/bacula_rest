__author__ = 'n0npax'

import bacula
from unittest import TestCase


class Test_logger(TestCase):

    def setUp(self):
        pass

    def test__exec_cmd(self):
        eng = bacula.Engine()
        output = eng._exec_cmd('help')
        self.assertEqual(True, 'Print help on specific command' in output)

    def test_version(self):
        eng = bacula.Engine()
        self.assertEqual(True, eng.show_version().startswith("Version"))
   
    def test_exec_cmd(self):
        eng = bacula.Engine()
        pools_list = eng.exec_cmd("list", "jobs")
        self.assertEqual(False, "error" in str(pools_list))


