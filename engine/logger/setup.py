import os
from setuptools import setup

setup(
    name = "logger",
    version = "0.1",
    author = "n0npax",
    author_email = "marcin.niemira@gmail.com",
    description = ("easy decorators for logging function executions and all Exceptions"),
    license = "BSD",
    packages=['loggert', 'test_logger'],
    classifiers=[
        "Development Status ::  Alpha",
        "Topic :: Liblaries",
        "License :: OSI Approved :: BSD License",
    ],
)
