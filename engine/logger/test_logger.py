__author__ = 'n0npax'
import logger
import os, time
from unittest import TestCase

test_log_path = logger.log_path

class Test_logger(TestCase):

    def setUp(self):
        with open(test_log_path, 'w') as f: f.write('')

    def test_log_warn(self):
        @logger.warn
        def fun(): raise Exception("Test Exception")
        try: fun() and self.fail()
        except:
            with open(test_log_path, 'r') as f: self.assertEquals(True, 'WARNING' in f.read())
 

    def test_log_info(self):
        @logger.info
        def fun(): pass
        try: fun()
        except: self.fail()
        finally:
            with open(test_log_path, 'r') as f: self.assertEquals(True, 'INFO' in f.read())

    def test_log_critical(self):
        @logger.critical
        def fun(): raise Exception("Test Exception")
        try: fun()
        except: pass
        finally:
            with open(test_log_path, 'r') as f: self.assertEquals(True, 'CRITICAL' in f.read())

    def test_log_error(self):
        @logger.error
        def fun(): raise Exception("Test Exception")
        try: fun()
        except: pass
        finally:
            with open(test_log_path, 'r') as f: self.assertEquals(True, 'ERROR' in f.read())

    def test_log_params(self):
        @logger.error
        def fun(): raise Exception("Test Exception")
        try: fun()
        except: pass
        finally:
            with open(test_log_path, 'r') as f:
                data = f.read()
                self.assertEquals(True, 'ERROR' in data)
                self.assertEquals(True, 'Test Exception' in data)
