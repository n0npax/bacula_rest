__author__ = 'n0npax'
import logging
import os

log_path = "/tmp/bacula-rest.log"

__logger = logging.getLogger(log_path)
logging.basicConfig(filename=log_path, level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def info(func):
    def func_wrapper(*args, **kwargs):
        try:
            __logger.info(func.__name__)
            return func(*args, **kwargs)
        except Exception as e:
            __logger.error(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper

def warn(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            __logger.warn(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper

def error(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            __logger.error(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper

def critical(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            __logger.critical(str(e)+'\targs: '+str(args)+'\tkwargs:'+str(kwargs))
            raise e
    return func_wrapper



