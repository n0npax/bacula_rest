__author__ = 'n0npax'

class Engine(object):

    @staticmethod
    def list_actions():
        _raise_not_implemented()

    @staticmethod
    def show_version():
        _raise_not_implemented()

def _raise_not_implemented():
    raise NotImplementedError("Abstract method. Has to be overwritten")

