__author__ = 'n0npax'
import logging
import abstract_engine
import logger.logger as logger
from subprocess import Popen, PIPE

allowed_cmd = {
    "list"     : ["copies", "files", "jobid=", "jobs", "jobtotals",  "media",  "pool=",  "pools", "volume" ],
    "add"      : ["jobid=", "pool=","storage="],
    "cancel"   : ["job=", "jobid=", "ujobid="],
    "create"   : [" pool="],
    "delete"   : ["job=", "pool=", "volume="],
    "disable"  : ["job="],
    "enable"   : ["job="],
    "estimate" : ["accurate=","client=","fileset=","job=","level="," listing"],
    "label"    : ["barcodes","pool=","slot=","storage=","volume="],
    "prune"    : ["client=","files","jobs","pool=","volume="],
    "purge"    : ["action=","allpools","devicetype=","drive=","files","jobs","pool=","storage=","volume="],
    "relabel"  : ["oldvolume=","pool=","storage=","volume="],
    "release"  : ["storage="],
    "run"      : ["client=","comment=","fileset=","job=","level=","storage=","when=","where=","yes"]
}

error_wrog_req = {
    "code"    :  "403",
    "message" :  "Not supported request ",
    "allowed" :  allowed_cmd,
    "status"  :  "error"
}

class Engine(abstract_engine.Engine):

    def __init__(self):
        pass

    @staticmethod
    def get_supported_command():
        return allowed_cmd

    @staticmethod
    def _exec_cmd(cmd):
        p_echo = Popen(["echo",cmd], stdout=PIPE)
        p_bconsole = Popen(["sudo","bconsole"], stdin=p_echo.stdout, stdout=PIPE)
        p_echo.stdout.close()
        raw_output = p_bconsole.communicate()[0]
        return raw_output
    
    @staticmethod   
    def show_version():
        raw_version = Engine._exec_cmd("version")
        return raw_version[raw_version.find("Version"):raw_version.find("(")]

    @staticmethod
    def list_actions():
        return ["get jobs", "list pools"]

    @staticmethod
    @logger.info
    def exec_cmd(par1='', par2='',par3='',par4='',par5='',par6=''):
        if par1 not in allowed_cmd: return error_wrog_req
        if par2 and par2 not in allowed_cmd[par1]: return error_wrog_req
        if par3 and par2[-1]!="=": return error_wrog_req
        if par2[-1]=="=": cmd = "{0} {1}{2} {3} {4} {5}".format(par1,par2,par3,par4,par5,par6)
        else: cmd="{0} {1} {2} {3} {4} {5}".format(par1,par2,par3,par4,par5,par6)
        raw_data = Engine._exec_cmd(cmd)
        if "|" in raw_data : return Engine._parse_bacula_output(raw_data.split('\n'))
        else: return raw_data

    @staticmethod
    def _parse_bacula_output(raw_data):
        item_lines =  [ line.split('|') for line in raw_data if line.startswith('|') ]
        items_list_header = [param_name.strip() for param_name in item_lines[0] if param_name.strip() not in ["",","]]
        items_list = []
        for item_str in item_lines[1:]:
            item_list=[]
            for item_param in item_str:
                if item_param.strip() in ["",","]:continue
                else: item_list.append(item_param.strip())
            items_list.append(dict(zip(items_list_header,item_list)))
        return items_list

       
